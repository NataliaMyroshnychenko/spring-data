package com.bsa.springdata.team;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TeamRepository extends JpaRepository<Team, UUID> {
    Optional<List<Team>> findByName(String teamName);

    int countByTechnologyName(String newTechnology);

    @Query("select distinct u from Team u " +
            "JOIN fetch u.technology t " +
            "JOIN fetch u.project proj " +
            "left JOIN u.users user on u.id = user.id " +
            "where t.name = :oldTechnologyName " +
            "and u.users.size < :devsNumber")
    List<Team> findTeamsByTechnologyNameAndUsersContainingLessThan(int devsNumber, String oldTechnologyName);

    @Transactional
    @Modifying
    @Query("update Team u set u.technology = :technology where u.technology.id = :idTech")
    void updateTeamById(Technology technology, UUID idTech);

    @Transactional
    @Modifying
    @Query(value = "update teams set name = :newTeamName where name = :hipsters", nativeQuery = true)
    void normalizeName(String newTeamName, String hipsters);

    @Query(value = "select team.name, concat(team.name, '_', project.name, '_', tech.name) as properties from teams team " +
            "LEFT JOIN  projects project on team.project_id = project.id " +
            "LEFT JOIN technologies tech on team.technology_id = tech.id " +
            "where team.name LIKE ?1", nativeQuery = true)
    String findNewTeamName(String hipsters);
}

