package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameContainingIgnoreCaseOrderByLastNameAsc(String lastName, Pageable pageable);

    @Query("select u from User u where u.office.city = :city order by u.lastName asc")
    List<User> findByCity(String city);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query("select u from User u where u.office.city = :city and u.team.room = :room order by u.lastName asc")
    List<User> findByRoomAndCity(String city, String room, Sort sort);

    int deleteByExperienceLessThan(int experience);
}
