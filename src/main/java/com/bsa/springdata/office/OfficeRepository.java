package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("SELECT DISTINCT u FROM Office u " +
            "join u.users users " +
            "WHERE users.team.technology.name = :technology")
    List<Office> getByTechnology(String technology);

    @Transactional
    @Modifying
    @Query("update Office u set u.address = :newAddress where u.address = :oldAddress and u.users.size > 0")
    void updateAddress(String oldAddress, String newAddress);

    Office findByAddress(String newAddress);
}
