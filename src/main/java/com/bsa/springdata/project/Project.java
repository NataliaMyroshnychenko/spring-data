package com.bsa.springdata.project;

import com.bsa.springdata.office.Office;
import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.user.User;
import com.bsa.springdata.user.dto.CreateUserDto;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "projects")
public class Project {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String name;

    @Column
    private String description;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Team> teams;

    public static Project fromDto(CreateProjectRequestDto project, List <Team> team) {
        return Project.builder()
                .id(UUID.randomUUID())
                .name(project.getProjectName())
                .description(project.getProjectDescription())
                .teams(team)
                .build();
    }
}
