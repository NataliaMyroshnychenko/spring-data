package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        List<Team> teamList = teamRepository.findTeamsByTechnologyNameAndUsersContainingLessThan(devsNumber, oldTechnologyName);
        teamList.forEach(x -> {
            Technology technology = x.getTechnology();
            technology.setName(newTechnologyName);
            x.setTechnology(technology);
            teamRepository.updateTeamById(technology, technology.getId());
        });
    }

    public void normalizeName(String hipsters) {
        String newTeamName = teamRepository.findNewTeamName(hipsters);
        teamRepository.normalizeName(newTeamName, hipsters);
    }
}
