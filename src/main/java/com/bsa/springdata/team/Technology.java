package com.bsa.springdata.team;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "technologies")
public class Technology {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String link;

    public static Technology fromDto(CreateProjectRequestDto project) {
        return Technology.builder()
                .id(UUID.randomUUID())
                .name(project.getTech())
                .description(project.getTechDescription())
                .link(project.getTechLink())
                .build();
    }
}
