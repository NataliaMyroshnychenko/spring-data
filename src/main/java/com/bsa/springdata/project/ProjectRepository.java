package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query("select u from Project u join u.teams teams where teams.technology.name = :technology order by teams.users.size DESC")
    List<Project> findTop5ByTechnology_NameOrderByTeamsDesc(String technology);

    @Query("select p from Project p " +
            "left join p.teams team " +
            "on p.id = team.id " +
            "order by p.teams.size desc, " +
            "team.users.size desc, " +
            "p.name desc")
    List<Project> findTheBiggest();

    @Query(value = "SELECT p.name as name, count(distinct t) as teamsNumber, count(u) as developersNumber, " +
            "STRING_AGG(distinct tech.name, ',' order by tech.name desc) as technologies " +
            "FROM projects p " +
            "LEFT JOIN teams t on p.id = t.project_id " +
            "LEFT JOIN users u on t.id = u.team_id " +
            "LEFT JOIN technologies tech on tech.id = t.technology_id " +
            "group by p.name " +
            "ORDER BY p.name ASC", nativeQuery = true)
    List<ProjectSummaryDto> getSummary();

    @Query("SELECT count(distinct u) FROM Project u " +
            "left join u.teams teams " +
            "left join teams.users users " +
            "left join users.roles role " +
            "WHERE role.name = :role")
    int getCountWithRole(String role);

}

